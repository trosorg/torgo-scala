# Torgo in Scala

This is a port of [torgo](http://tros.org/torgo) utilizing [Scala](scala-lang.org). The purpose of this project is to simply learn Scala. In turn, this may allow for future expandability of torgo into the realm of lisp.

As this is an exercise in learning Scala, the goal is not to pull over all Java code, but instead to re-write the source in Scala.

## Current State

1. Can parse logo code
2. Can perform programmatic mathematic operations

## TODO

1. Functions
2. Most of the Logo functionality
3. Canvas/UI
4. Pretty much everything

## Proof of Concepts

1. Can use Scala to analyze script
