/*
 * Copyright 2015-2016 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tros.scala

import org.antlr.v4.runtime.{ ANTLRInputStream, CommonTokenStream }
import org.tros.logo.antlr.{ LogoLexer, LogoParser }
import org.tros.scala.torgo.{ DynamicScope, LexicalScope, CodeBlock, InterpreterValue, NumberType }
import org.tros.scala.logo.{ ExpressionListener, LogoCanvas }
import scala.collection.mutable.{ ListBuffer }

object Main {
  def main(args: Array[String]): Unit = {
    val f = new ANTLRInputStream(getClass.getResourceAsStream("/logo/examples/antlr/procedure2.txt"))
    val parser = new LogoParser(new CommonTokenStream(new LogoLexer(f)))
    val tree = parser.prog()
    val ll = new org.tros.scala.logo.LexicalListener(new LogoCanvas(), tree)
    val enter = ll.entryPoint()

    val scope = new DynamicScope()
    val x = new CodeBlock()
    val y = new CodeBlock()
    y.parent(x)
    scope.push(new CodeBlock())
    scope.push(new CodeBlock())

    scope.set("a", new InterpreterValue(5, org.tros.scala.torgo.NumberType))
    println(scope.has("a"))
    scope.pop()
    println(scope.has("a"))
    scope.pop()

    /**
     * Since CodeBlock is not fully implemented, this will run, but not do anything.
     */
    val scope2 = new LexicalScope()
    scope2.push(new CodeBlock())
    scope2.push(new CodeBlock())
    scope2.set("a", new InterpreterValue())
    println(scope2.has("a"))
    scope2.pop()
    println(scope2.has("a"))
    scope2.pop()

    val stack = ListBuffer[Int](1)
    stack += 2
    println(stack)

    val val1 = new InterpreterValue(3.3, NumberType)
    val val2 = new InterpreterValue(2.2, NumberType)
    val l = List[InterpreterValue](val1, val2)
    println(l.map(_.value.asInstanceOf[Double]).sum)
    
    val ex = new ExpressionListener(scope, tree)
    println(ex.mathExpression(val1, val2, "+").value)
    println(ex.mathExpression(val1, val2, "-").value)
    println(ex.mathExpression(val1, val2, "*").value)
    println(ex.mathExpression(val1, val2, "/").value)
    println(ex.mathExpression(val1, val2, "\\").value)
    println(ex.mathExpression(val1, val2, "^").value)
    println(ex.mathExpression(val1, val2, "%").value)
  }
}
