/*
 * Copyright 2015-2016 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tros.scala.logo

import org.tros.scala.torgo.{ Scope, InterpreterValue, NumberType, StringType }
import org.antlr.v4.runtime.tree.{ ParseTree, ParseTreeWalker }
import org.antlr.v4.runtime.{ ParserRuleContext }
import scala.collection.mutable.{ Stack, ListBuffer }

/**
 * TODO: Drop the 'evaluate' function for just doing the work in the contructor.
 */
class ExpressionListener(scope: Scope, tree: ParseTree) extends org.tros.logo.antlr.LogoBaseListener {

  val value = Stack[ListBuffer[InterpreterValue]]()

  def evaluate(): InterpreterValue = {
    ParseTreeWalker.DEFAULT.walk(this, tree)
    value.head.head
  }

  def getValue(): InterpreterValue = {
    value.head.head;
  }

  def mathExpression(val1: InterpreterValue, val2: InterpreterValue, op: String): InterpreterValue = {
    val num1 = val1.value.asInstanceOf[Double]
    val num2 = val2.value.asInstanceOf[Double]
    val x = op match {
      case "+" => num1 + num2
      case "-" => num1 - num2
      case "*" => num1 * num2
      case "/" => num1 / num2
      case "\\" => scala.math.round(num1 / num2)
      case "%" => num1 % num2
      case "^" => scala.math.pow(num1, num2)
    }
    new InterpreterValue(x, val1.valueType)
  }

  override def enterExpression(ctx: org.tros.logo.antlr.LogoParser.ExpressionContext): Unit = {
    value.push(ListBuffer[InterpreterValue]())
  }

  def __innerExit(index: Int, values: ListBuffer[InterpreterValue], ctx: ParserRuleContext): InterpreterValue = {
    if (index < ctx.getChildCount()) {
      //pop
      //pop
      val v: InterpreterValue = mathExpression(values.remove(0), values.remove(0), ctx.getChild(index).getText())
      //push
      values.insert(0, v)
      __innerExit(index + 2, values, ctx)
    } else {
      //pop last remaining value
      values.remove(0)
    }
  }

  override def exitExpression(ctx: org.tros.logo.antlr.LogoParser.ExpressionContext): Unit = {
    //pop and enqueue
    value.head += __innerExit(1, value.pop(), ctx)
  }

  override def enterDeref(ctx: org.tros.logo.antlr.LogoParser.DerefContext): Unit = {
    val ref = scope.get(ctx.name.STRING.getText)
    value.head += ref
  }

  override def enterNumber(ctx: org.tros.logo.antlr.LogoParser.NumberContext): Unit = {
    val ref = ctx.NUMBER.getSymbol.getText.toDouble
    value.head += new InterpreterValue(ref, NumberType)
  }

  override def enterMultiplyingExpression(ctx: org.tros.logo.antlr.LogoParser.MultiplyingExpressionContext): Unit = {
    value.push(ListBuffer[InterpreterValue]())
  }

  override def exitMultiplyingExpression(ctx: org.tros.logo.antlr.LogoParser.MultiplyingExpressionContext): Unit = {
    //pop and enqueue
    value.head += __innerExit(1, value.pop(), ctx)
  }

  override def enterPowerExpression(ctx: org.tros.logo.antlr.LogoParser.PowerExpressionContext): Unit = {
    value.push(ListBuffer[InterpreterValue]())
  }

  override def exitPowerExpression(ctx: org.tros.logo.antlr.LogoParser.PowerExpressionContext): Unit = {
    //pop and enqueue
    value.head += __innerExit(1, value.pop(), ctx)
  }

  override def enterRandom(ctx: org.tros.logo.antlr.LogoParser.RandomContext): Unit = {
    value.push(ListBuffer[InterpreterValue]())
  }

  override def exitRandom(ctx: org.tros.logo.antlr.LogoParser.RandomContext): Unit = {
    val values = value.pop()
    val max = values(0).value.asInstanceOf[Double].toInt
    val ran = scala.util.Random.nextInt(max)
    value.head += new InterpreterValue(ran.toDouble, NumberType)
  }

  override def exitSignExpression(ctx: org.tros.logo.antlr.LogoParser.SignExpressionContext): Unit = {
    val sign = ctx.getChild(0).getText;
    val peek = value.head;
    val index = peek.size - 1;
    if (peek(index).valueType == NumberType) {
      val ref = peek.remove(index);
      var o = ref.value.asInstanceOf[Double];
      if ("-" == sign) {
        o = o * -1;
      }
      peek.insert(index, new InterpreterValue(o, NumberType));
    }
  }

  override def enterValue(ctx: org.tros.logo.antlr.LogoParser.ValueContext): Unit = {
    if (ctx.STRINGLITERAL != null) {
      value.head += new InterpreterValue(ctx.STRINGLITERAL.getText.substring(1), StringType);
    }
  }
}
