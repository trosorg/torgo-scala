/*
 * Copyright 2015-2016 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tros.scala.logo

import org.antlr.v4.runtime.tree.{ ParseTree, ParseTreeWalker }
import org.tros.scala.torgo.{ LexicalAnalyzer, CodeBlock }
import scala.collection.mutable.{ Stack, ListBuffer }

/**
 * TODO: see if functional programming can take care of some of the duplicate
 * code from the below functions.
 */
class LexicalListener(canvas: LogoCanvas, tree: ParseTree) extends org.tros.logo.antlr.LogoBaseListener with LexicalAnalyzer {

  val stack = Stack[CodeBlock]()
  val blocks = ListBuffer[CodeBlock]()
  ParseTreeWalker.DEFAULT.walk(this, tree)

  def entryPoint(): CodeBlock = {
    stack.head
  }

  def codeBlocks(): Iterable[CodeBlock] = {
    blocks
  }

  override def enterProcedureDeclaration(ctx: org.tros.logo.antlr.LogoParser.ProcedureDeclarationContext): Unit = {
    //simple debug statment...
    println("found function: " + ctx.name.getText)
    val lf = new LogoFunction(ctx.name.getText, ctx)
    blocks += lf
    stack.head.addFunction(lf)
    lf.parent(stack.head)
    stack.push(lf)
  }

  override def exitProcedureDeclaration(ctx: org.tros.logo.antlr.LogoParser.ProcedureDeclarationContext): Unit = {
    stack.pop()
  }

  override def enterDs(ctx: org.tros.logo.antlr.LogoParser.DsContext): Unit = {
    val logoStatement = new LogoStatement("ds", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterCc(ctx: org.tros.logo.antlr.LogoParser.CcContext): Unit = {
    val logoStatement = new LogoStatement("cc", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterPc(ctx: org.tros.logo.antlr.LogoParser.PcContext): Unit = {
    val logoStatement = new LogoStatement("pc", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterFontname(ctx: org.tros.logo.antlr.LogoParser.FontnameContext): Unit = {
    val logoStatement = new LogoStatement("fontname", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterFontstyle(ctx: org.tros.logo.antlr.LogoParser.FontstyleContext): Unit = {
    val logoStatement = new LogoStatement("fontstyle", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterFontsize(ctx: org.tros.logo.antlr.LogoParser.FontsizeContext): Unit = {
    val logoStatement = new LogoStatement("fontsize", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterProg(ctx: org.tros.logo.antlr.LogoParser.ProgContext): Unit = {
    stack.push(new LogoProg(ctx))
  }

  override def enterPrint(ctx: org.tros.logo.antlr.LogoParser.PrintContext): Unit = {
    val logoStatement = new LogoStatement("print", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterFd(ctx: org.tros.logo.antlr.LogoParser.FdContext): Unit = {
    val logoStatement = new LogoStatement("fd", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterBk(ctx: org.tros.logo.antlr.LogoParser.BkContext): Unit = {
    val logoStatement = new LogoStatement("bk", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterRt(ctx: org.tros.logo.antlr.LogoParser.RtContext): Unit = {
    val logoStatement = new LogoStatement("rt", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterLt(ctx: org.tros.logo.antlr.LogoParser.LtContext): Unit = {
    val logoStatement = new LogoStatement("lt", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterPu(ctx: org.tros.logo.antlr.LogoParser.PuContext): Unit = {
    val logoStatement = new LogoStatement("pu", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterPd(ctx: org.tros.logo.antlr.LogoParser.PdContext): Unit = {
    val logoStatement = new LogoStatement("pd", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterCs(ctx: org.tros.logo.antlr.LogoParser.CsContext): Unit = {
    val logoStatement = new LogoStatement("cs", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterHt(ctx: org.tros.logo.antlr.LogoParser.HtContext): Unit = {
    val logoStatement = new LogoStatement("ht", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterSt(ctx: org.tros.logo.antlr.LogoParser.StContext): Unit = {
    val logoStatement = new LogoStatement("st", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterHome(ctx: org.tros.logo.antlr.LogoParser.HomeContext): Unit = {
    val logoStatement = new LogoStatement("home", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterSetxy(ctx: org.tros.logo.antlr.LogoParser.SetxyContext): Unit = {
    val logoStatement = new LogoStatement("setxy", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterProcedureInvocation(ctx: org.tros.logo.antlr.LogoParser.ProcedureInvocationContext): Unit = {
    val logoStatement = new LogoStatement(ctx.name.getText, ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterMake(ctx: org.tros.logo.antlr.LogoParser.MakeContext): Unit = {
    val logoStatement = new LogoStatement("make", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterLocalmake(ctx: org.tros.logo.antlr.LogoParser.LocalmakeContext): Unit = {
    val logoStatement = new LogoStatement("localmake", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterIfe(ctx: org.tros.logo.antlr.LogoParser.IfeContext): Unit = {
    //LogoIf ife = new LogoIf(ctx.comparison())
    val lc = new LogoIf(ctx)
    blocks += lc
    stack.head.addCommand(lc)
    lc.parent(stack.head)
    stack.push(lc)
  }

  override def exitIfe(ctx: org.tros.logo.antlr.LogoParser.IfeContext): Unit = {
    stack.pop()
  }

  override def enterStop(ctx: org.tros.logo.antlr.LogoParser.StopContext): Unit = {
    val logoStatement = new LogoStatement("stop", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def enterFore(ctx: org.tros.logo.antlr.LogoParser.ForeContext): Unit = {
    val lc = new LogoFor(ctx)
    blocks += lc
    stack.head.addCommand(lc)
    lc.parent(stack.head)
    stack.push(lc)
  }

  override def exitFore(ctx: org.tros.logo.antlr.LogoParser.ForeContext): Unit = {
    stack.pop()
  }

  override def enterRepeat(ctx: org.tros.logo.antlr.LogoParser.RepeatContext): Unit = {
    val lc = new LogoRepeat(ctx)
    blocks += lc
    stack.head.addCommand(lc)
    lc.parent(stack.head)
    stack.push(lc)
  }

  override def enterPause(ctx: org.tros.logo.antlr.LogoParser.PauseContext): Unit = {
    val logoStatement = new LogoStatement("pause", ctx, canvas)
    blocks += logoStatement
    stack.head.addCommand(logoStatement)
  }

  override def exitRepeat(ctx: org.tros.logo.antlr.LogoParser.RepeatContext): Unit = {
    stack.pop()
  }
}
