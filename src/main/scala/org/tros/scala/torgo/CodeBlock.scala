/*
 * Copyright 2015-2016 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tros.scala.torgo

import org.antlr.v4.runtime.ParserRuleContext
import scala.collection.mutable.{ ListBuffer, HashMap }

class CodeBlock(ctx: ParserRuleContext = null) {
  
  private var _par:CodeBlock = null
  val listeners = ListBuffer[InterpreterListener]()
  val commands = ListBuffer[CodeBlock]()
  val functions = HashMap[String, CodeFunction]()

  def parserRuleContext() : ParserRuleContext = {
    ctx
  }
  
  def parent(): CodeBlock =
    _par

  def parent(p: CodeBlock): Unit =
    _par = p

  def addCommand(command: CodeBlock): Unit = {
    commands += command
  }

  def addCommand(command: Iterable[CodeBlock]): Unit = {
    commands ++= command
  }
  
  def addListener(listener: InterpreterListener) =
    listeners += listener

  def removeListener(listener: InterpreterListener) =
    listeners -= listener

  def hasVariable(name: String): Boolean =
    false

  def getVariable(name: String): InterpreterValue =
    null

  def setVariable(name: String, value: InterpreterValue): Unit = {}

  def localVariables(): Iterable[String] =
    null

  def addFunction(fn: CodeFunction): Unit = {
    functions(fn.name) = fn
  }
}
