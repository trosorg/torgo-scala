/*
 * Copyright 2015-2016 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tros.scala.torgo

import scala.collection.mutable.{ HashMap, Stack }

class DynamicScope extends Scope {

  val scope = Stack[HashMap[String, InterpreterValue]]()

  override def push(block: CodeBlock) = {
    scope.push(HashMap[String, InterpreterValue]())
    super.push(block)
  }

  override def pop() = {
    scope.pop()
    super.pop()
  }

  def has(name: String): Boolean = {
    val filt = scope.filter(
      _.filterKeys(_ == name).size > 0
    )
    filt.size > 0
  }

  def get(name: String): InterpreterValue = {
    val filt = scope.filter(
      _.filterKeys(_ == name).size > 0
    )
    if (filt.size > 0) {
      return filt.head(name)
    } else {
      return null
    }
  }

  def set(name: String, value: InterpreterValue): Unit = {
    val filt = scope.filter(
      _.filterKeys(_ == name).size > 0
    )
    if (filt.size > 0) {
      filt.head(name) = value
    } else {
      scope.head(name) = value
    }
  }

  def setNew(name: String, value: InterpreterValue): Unit = {
    scope.head(name) = value
  }

  def getFunction(name: String): CodeBlock = {
    /**
     * Filter
     */
    new CodeBlock()
  }

  def hasFunction(name: String): Boolean =
    /**
     * Filter
     */
    false

  def localVariables(): Iterable[String] = {
    scope.head.keys
  }
}
