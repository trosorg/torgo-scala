/*
 * Copyright 2015-2016 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tros.scala.torgo

class LexicalScope extends Scope {

  def has(name: String): Boolean = {
    def hasVariableInner(name: String, curr: CodeBlock): Boolean = {
      if (curr != null) {
        if (curr.hasVariable(name)) {
          true
        } else {
          hasVariableInner(name, curr.parent)
        }
      } else {
        false
      }
    }
    hasVariableInner(name, stack.head)
  }

  def get(name: String): InterpreterValue = {
    def getVariableInner(name: String, curr: CodeBlock): InterpreterValue = {
      if (curr != null) {
        if (curr.hasVariable(name)) {
          curr.getVariable(name)
        } else {
          getVariableInner(name, curr.parent)
        }
      } else {
        null
      }
    }
    getVariableInner(name, stack.head)
  }

  def set(name: String, value: InterpreterValue): Unit = {
    def setVariableInner(name: String, curr: CodeBlock): Boolean = {
      if (curr != null) {
        if (curr.hasVariable(name)) {
          curr.setVariable(name, value)
          true
        } else {
          setVariableInner(name, curr.parent)
        }
      } else {
        false
      }
    }
    if (!setVariableInner(name, stack.head)) {
      stack.head.setVariable(name, value)
    }
  }

  def setNew(name: String, value: InterpreterValue): Unit = {
    stack.head.setVariable(name, value)
  }

  def getFunction(name: String): CodeBlock = {
    /**
     * Filter
     */
    new CodeBlock()
  }

  def hasFunction(name: String): Boolean =
    /**
     * Filter
     */
    false

  def localVariables(): Iterable[String] = {
    stack.head.localVariables
  }
}
