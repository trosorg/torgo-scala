/*
 * Copyright 2015-2016 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tros.scala.torgo

import scala.collection.mutable.{ ListBuffer, Stack }

trait Scope {

  val stack = Stack[CodeBlock]()
  val listeners = ListBuffer[ScopeListener]()

  def addListener(listener: ScopeListener) =
    listeners += listener

  def removeListener(listener: ScopeListener) =
    listeners -= listener

  def get(name: String): InterpreterValue

  def has(name: String): Boolean

  def pop() =
    stack.pop()

  def peek(index: Int) =
    stack(index)

  def peek() =
    stack.head

  def push(block: CodeBlock) =
    stack.push(block)

  def size() =
    stack.length

  def set(name: String, value: InterpreterValue): Unit

  def setNew(name: String, value: InterpreterValue): Unit

  def getFunction(name: String): CodeBlock

  def hasFunction(name: String): Boolean

  def localVariables(): Iterable[String]

}
